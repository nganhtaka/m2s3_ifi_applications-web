# IFI - Applications Web

### Thi-Ngoc-Anh TRAN

### Master 2 - E-Service - Informatique - Universite de Lille

- Cours : https://les-tilleuls-coop.slides.com/masterclass/php/live

- test code php : https://3v4l.org/

### I. Introduction
- telnet <'site'> <'port'> : ex 'telnet google.com 80'. Puis 'GET /' or 'POST /bonjour.php'

- Version php actuel 7.2.24
- Lancer le serveur : `php -S localhost:8080`

- Aller sur le lien : http://localhost:8080/doc.html ou http://localhost:8080/bonjour.php

- show info : phpinfo();
- show navigateur : `($_SERVER['HTTP_USER_AGENT'])`. Peut trouver dans phpinfo/PHP variables.
- A ?? B = si A undefined, show B

### II. show error dans le console : 
- dans php.ini, mettre :  `error_reporting = E_ALL` 
- ou directement dans fichier php : `error_reporting(E_ALL)` et/ou `ini_set('display_errors', 1)`

### III. Trois manières d'envoyer des données :

<details><summary>Details</summary>

1. url query : abc.com?param=un&autre=param
    - `echo 'Bonjour ',$_GET['firstname']`;
    - http://localhost:8080/queries.php?firstname=Taka
    - Mais on peut avoir l'erreur si param firstname n'est pas dans l'url. Donc : `echo 'Bonjour ',($_GET['firstname'] ?? 'unknown')`;
    - par ex: si on faire firstname est un script : `http://localhost:8080/queries.php?firstname=<0script>alert('test')</script>`, le code va être éxécuter. Pour éviter : **htmlspecialchars**.

2. Post via entrée standard : les entêtes
    - Lancer la commande : `curl -X POST http://localhost:8080/queries.php` pour vérifier le résultat reçu. Sinon on peut utiliser postman
    - Dans le console du navigateur/network : clique droit sur queries.php -> copy -> copy as cURL puis paste sur le terminal, par ex `curl 'http://localhost:8080/queries.php' -H 'Connection: keep-alive' ... Linux x86_64) ... Chrome/78.0.3904.97 Safari/537.36' ...'Accept-Language: en-US,en..`
    - Pour ajoute un retour à la ligne : `echo $_SERVER['REQUEST_METHOD'].PHP_EOL;`
    - Lancer la commande : `curl -X POST http://localhost:8080/queries.php?firstname=Taka`

3. Post via entrée standard : les variables d'environnements (form ?)

</details>

### IV. Executer un programme

<details><summary>Details</summary>

- Depuis de commande : dans le terminal 
    + exécuter le script contenu dans le fichier myprog : `php myprog.php`
    + exécuter php en mode interactif : `php -a`. Par ex:
        ```
        php > echo "test";
        test
        php > $myVar=1;
        php > var_dump($myVar);
        int(1)
        ```
    + Site verif si le code marche bien sur les versions php : https://3v4l.org/.
    + lance un serveur php local sur localhost, et chargera le fichier myprog.php du répertoire : `php -S localhost:8000 -t path/to/directory`
    + lance un serveur php local sur localhost et chargera le fichier file.php du répertoire `php -S localhost:8000 path/to/file.php`

</details>

### V. Les types :

<details><summary>Details</summary>

- Type int
    ```
    php > $foo=1;
    php > var_dump($foo);
    int(1)
    ```

- Type String
    ```
    php > $foo2 = "hello";
    php > var_dump($foo2);
    string(5) "hello"
    ```

- cast int en string
    ```
    php > $foo3 = (string) $foo;
    php > var_dump($foo3);
    string(1) "1"
    ```
- Comparer int
    ```
    php > $age = 16;
    php > echo ($age >16) ? "plus" : "moins";
    moins
    php > echo ($age >12) ? "plus" : "moins";
    plus
    ```

- Callable 
    ```php
    function maFonction($param1) {
        echo $param1, PHP_EOL;
    }
    maFonction('bonjour');                  // bonjour
    var_dump(is_callable('maFonction'));    // bool(true)

    $maFonction2 = function() {};
    var_dump(is_callable('maFonction2'));   // bool(false)
    ```

</details>

### VI. Array, Map :

<details><summary>Details</summary>

- array
    ```php
    $tab=[4,7,9];
    $tab[]=22;
    var_dump(count($tab));
    foreach($tab as $value) {
        var_dump($value);
    }
    ```

    ```
    output: 
    int(4)
    int(4)
    int(7)
    int(9)
    int(22)
    ```

- map
    ```php
    $map=[
        'clef' => 'value',
        'clef2' => 'value2'
        ];
    foreach($map as $k => $v) {
        var_dump($k, $v);
        echo '---', PHP_EOL;
    }
    ```

    ```
    output :     
    string(4) "clef"
    string(5) "value"
    ---
    string(5) "clef2"
    string(6) "value2"
    ---
    ```

- map en object
    ```php
    $map=[
        'clef' => 'value',
        'clef2' => 'value2'
        ];
    var_dump((object) $map);
    ```

    ```
    object(stdClass)#1 (2) {
    ["clef"]=>
    string(5) "value"
    ["clef2"]=>
    string(6) "value2"
    }
    ```

    ```php
    $obj = new stdClass();
    $obj->prop1 = 'a';
    $obj->prop2 = 'b';
    var_dump($obj);

    var_dump((array) $obj);
    ```

    ```
    object(stdClass)#1 (2) {
    ["prop1"]=>
    string(1) "a"
    ["prop2"]=>
    string(1) "b"
    }
    array(2) {
    ["prop1"]=>
    string(1) "a"
    ["prop2"]=>
    string(1) "b"
    }
    ```

</details>

### VII. String et Text:

<details><summary>Details</summary>

- cast string en txt
    ```php
    $myInt = '17ans';
    var_dump($myInt);           // string(5) "17ans"
    var_dump((int) $myInt);     // int(17)
    ```

    ```php
    var_dump(12 == 12);      // bool(true)
    var_dump(12 == 12.0);    // bool(true)
    ```

    ```php
    var_dump(0 == 0);       // bool(true)
    var_dump(0 == "");      // bool(true)
    var_dump(0 == false);   // bool(true)

    var_dump(0 === 0);      // bool(true)
    var_dump(0 === "");     // bool(false)
    var_dump(0 === false);  // bool(false)
    var_dump("" === false); // bool(false)
    ```
    
    ```php
    $nom='Kévin';
    var_dump($nom ?? 'nom par défaut');     // string(6) "Kévin"
    var_dump($nom2 ?? 'nom par défaut');    // string(15) "nom par défaut"
    ```

- condition ?: et ??
    ```php
    $prenom = null;
    echo "bonjour ", ($prenom ?: 'inconnu. ');  // bonjour inconnu.

    $prenom = "test";
    echo "bonjour ", ($prenom ?: 'inconnu. ');  // bonjour test

    $prenom = "";
    echo "bonjour ", ($prenom ?: 'inconnu. ');  // bonjour inconnu.

    $prenom = "";
    echo "bonjour ", ($prenom ?? 'inconnu. ');  // bonjour 
    ```

</details>

### VIII. Tableaux :

<details><summary>Details</summary>

- Supprimer un element et property d'un element 

    ```php
    <?php

    $assoc = [
        'kevin' => ['pass' => 'abc'],
        'nico' => ['pass' => 'def'],
        ];
    var_dump(count($assoc));
        
    unset($assoc['nico']);
    var_dump(count($assoc));

    var_dump($assoc['kevin']);
    unset($assoc['kevin']['pass']);
    var_dump($assoc['kevin']);
    ```

- Merge 2 arrays

    ```php
    <?php

    $array1 = [0 => 1, 1 => 2, 2 => 3];
    $array2 = [0 => 3, 1 => 5, 2 => 5];

    var_dump($array1 + $array2);  // remplace array2 to array1
    var_dump(array_merge($array1, $array2));    // merge 2 array
    ```

- keys et values d'un array

    ```php
    <?php

    $password = [
            'kevin' => 'kpass',
            'nico' => 'npass',
            ];
    var_dump(array_keys($password));        // array(2) {[0]=>string(5) "kevin"  [1]=>string(4) "nico"}
    var_dump(array_values($password));      // array(2) {[0]=>string(5) "kpass"  [1]=>string(5) "npass"}
    ```

- Array en string avec implode
    ```php
    <?php

    $assignAlimentaire = [
            'personne_1' => 'banane',
            'personne_2' => 'pomme',
            ];

    $usernames = implode(',', array_keys($assignAlimentaire));
    echo "La liste des utilisateurs : $usernames";
    // La liste des utilisateurs : personne_1,personne_2
    ```

</details>

### IX. Function

<details><summary>Details</summary>

    ```php
    <?php
 
    echo "je suis ligne 3"."\n";

    $date = date("m-d-Y");
    $myFunction = function() use ($date) {
        echo "Bonjour, nous somme le ".$date."\n";
    };

    class Controller {
        public function renderElement(callable $ĥtmlGenerator) {
            echo "Je suis ligne 12"."\n";
            
            $ĥtmlGenerator();
        }
    }

    $c = new Controller();
    $c->renderElement($myFunction);
    echo "je suis ligne 20"."\n";

    // resultat 
    // je suis ligne 3
    // Je suis ligne 12
    // Bonjour, nous somme le 11-26-2019
    // je suis ligne 20
    ```

</details>

### X. Réference

- C'est comme pointer dans C : ajoute & devant la variable : ex : `&$value`. Mais c'est dangereux, à cause de l'effet de bord --> à EVITER

### XI. cookie et session

<details><summary>Details</summary>

    ```php
    ```

</details>

    