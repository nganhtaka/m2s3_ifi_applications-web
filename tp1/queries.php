<?php
    // II
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    // III.1
    // echo 'Bonjour ',$_GET['firstname'];
    // echo 'Bonjour ',($_GET['firstname'] ?? 'unknown');
    // echo 'Bonjour ',htmlspecialchars($_GET['firstname'] ?? 'unknown');

    // III.2
    echo $_SERVER['REQUEST_METHOD'].PHP_EOL;
    echo 'Bonjour ',htmlspecialchars($_GET['firstname'] ?? 'unknown');
    