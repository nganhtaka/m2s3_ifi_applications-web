<?php

    header('Content-Type: text/html; charset=utf-8');
    header('Foo : Bar');
    // lancer avec cmd ; php -S localhost:8080
    // test avec url ; http://localhost:8000/exo1.php?username=Anh
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php
    $username = $_GET['username'] ?? 'inconnu';
?>

<meta charset="utf-8">
</head>
<!-- <title>My Page de  <?php echo $username ?></title> -->
<title>My Page de <?=htmlspecialchars($username) ?></title>

<body>
<!-- <h1>Hello <?php echo $username ?></h1> -->
<h1>Hello <?=htmlspecialchars($username) ?></h1>
</body>
</html>