<!-- Créer un tableau de personnes en leur assignant un aliment.
stocker un nom dans une variable, si cette personne existe dans le tableau afficher la phrase: "{NOM} aime manger {ALIMENT}"
En remplaçant les crochets par les valeurs. 

Test avec : http://localhost:8000/exo3.php -->

<?php

$assignAlimentaire = [
        'personne_1' => 'banane',
        'personne_2' => 'pomme',
        ];

$maFonction = function ($param, $array) {
    if (array_key_exists($param, $array)) {
        echo "$param aime manger $array[$param] <br/>";
    } else {
        echo "$param existe pas <br/>";
    }
};

$username = 'test';
$maFonction($username, $assignAlimentaire);

$username = 'personne1';
$maFonction($username, $assignAlimentaire);