<?php
 
$start = 4;
 
$total = 0;
$numbers = [];
 
/* 	Écrire un programme qui parcours les nombres et 
   	additionne les chiffres pairs avec la valeur de départ, 
    puis affiche le total obtenu. */

$counter=0;
while ($counter<11) {
    $numbers[] = $counter;
    $counter++;
}

$total = $start;
foreach($numbers as $value) {
    if (($value % 2) === 0) $total+= ($start + $value);
}
echo "Total is $total.";