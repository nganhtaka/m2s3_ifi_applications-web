<!-- Déplacer la fonction sum dans un second fichier php, et l'importer avec la fonction include.
Essayer de l'importer une seconde fois
Trouver un moyen de ne plus avoir l'erreur en important 2x le fichier. -->

<?php

// donne l'erreur si on l'importe 2 fois include 'sum.php'; 
// donc utilise include_once pour éviter

include_once 'sum.php';
sum(3,5,7,11,13);

include_once 'sum.php';

